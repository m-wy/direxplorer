mod directory;
use directory::Directory;

extern crate clap;
use std::path::Path;
use clap::{App, Arg};

extern crate fs_extra;
use fs_extra::dir::*;

use std::collections::{HashSet};

fn main() -> std::io::Result<()> {
    let args = App::new("Heimdall")
        .version("0.1.0")
        .author("Mathieu W. <matt.willay@gmail.com>")
        .help_message("FileSystem Explorer")
        .arg(Arg::with_name("depth")
            .takes_value(true)
            .help("The depth of file system exploration")
            .short("d")
            .long("depth")
            .multiple(false)
            .required(false)
        )
        .get_matches();

    // PARSING INPUT
    let mut depth = 1;
    let mut depth_input = "";
    if let Some(x) = args.value_of("depth") { depth_input = x }
    if !depth_input.is_empty(){
        match depth_input.parse() {
            Ok(x) => depth = x,
            Err(_) => panic!("Invalid input for parameter depth (-d or --depth)")
        }
    }

    // GETTING ROOT DIR INFOS
    let root_dir = explore_dir(Path::new("."), &DirOptions{depth : 1});
    println!("{:?}", root_dir);
    Ok(())
}

fn explore_dir(path : &Path, options : &DirOptions) -> Directory {
    let content;
    match get_dir_content2(path, options) {
        Ok(x) => {content = x;}
        Err(_) => {content = DirContent{
            dir_size: 0,
            files: vec![],
            directories: vec![]
        };}
    }
    let mut children = vec![];
    for dir_index in 1..content.directories.len() {
        let path_child = Path::new(&content.directories[dir_index]);
        children.insert(children.len(),explore_dir(path_child, options));
    }
    let mut curr_dir = get_dirs(path);
    curr_dir.directories = children;
    curr_dir
}

fn get_dirs(path : &Path) -> Directory{
    let mut config = HashSet::new();
    config.insert(DirEntryAttr::Name);
    config.insert(DirEntryAttr::Size);
    println!("{:?}", path);
    let entry_infos = get_details_entry(path, &config).unwrap();
    let mut name = "".to_string();
    let mut size = 0;
    for entry_info in entry_infos {
        match entry_info.0 {
            DirEntryAttr::Name => if let DirEntryValue::String(s) = entry_info.1 {name = s},
            DirEntryAttr::Size => if let DirEntryValue::U64(x) = entry_info.1 {size = x},
            _ => ()
        }
    }
    Directory{
        directories: vec![],
        name,
        size
    }
}

#[test]
fn test_folder_is_correctly_explored() {
    let test_dir = explore_dir(Path::new(".\\testfolder"), &DirOptions{depth : 1});
    let expected_directory = Directory{
        name: "testfolder".to_string(),
        size: 0,
        directories: vec![Directory{
                            name: "x".to_string(),
                            size: 0,
                            directories: vec![]
                        },Directory{
                            name: "y".to_string(),
                            size: 0,
                            directories: vec![Directory{
                                                name: "z".to_string(),
                                                size: 0,
                                                directories: vec![]
                                            },Directory{
                                                name: "zz".to_string(),
                                                size: 0,
                                                directories: vec![]
                                            }]
        }]
    };
    assert_eq!(test_dir, expected_directory);
}