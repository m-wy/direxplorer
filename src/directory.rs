#[derive(Debug, Eq, PartialEq)]
pub struct Directory{
    pub(crate) name : String,
    pub(crate) size : u64,
    pub(crate) directories : Vec<Directory>
}
//TODO add % of parent size in print
